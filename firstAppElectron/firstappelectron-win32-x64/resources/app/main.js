const { app, BrowserWindow } = require('electron');
if (process.versions && process.versions.electron) {
	process.env.NODE_CONFIG_DIR = require('electron').app.getAppPath() + '/config';
  }
 
const config = require('config');
const fs = require('fs');

console.log(process.cwd(), __dirname)

let win, win2;

function createWindow() {
	win = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			nodeIntegration: true
		}
	});
	win2 = new BrowserWindow({
		width: 400,
		height: 400,
		webPreferences: {
			nodeIntegration: true
		}
	});
	console.log('up');
	console.log(process.cwd(), __dirname,"jj")
	let data = `
CLINIC_ID: ${config.get('CLINIC_ID')}
WAKEUP_INTERVAL: ${config.get('WAKEUP_INTERVAL')}
PRIIMO_HOST_ADDRESS: ${config.get('PRIIMO_HOST_ADDRESS')}
PRIIMO_HOST_TOKEN: ${config.get('PRIIMO_HOST_TOKEN')}
SHARED_DIR_LOCATIONS: ${config.get('SHARED_DIR_LOCATIONS')};
PROVIDER_NAME: ${config.get('PROVIDER_NAME')}
TRASH_DIR: ${config.get('TRASH_DIR')}
RETRY_PERIOD: ${config.get('RETRY_PERIOD')}
CLEAR_SHARED_DIR: ${config.get('CLEAR_SHARED_DIR')}
FILES_MASK: ${config.get('FILES_MASK')}
LOG_FILE_RETENTION_PERIOD: ${config.get('LOG_FILE_RETENTION_PERIOD')}
`;

	win.loadFile('C:\\Users\\or\\Downloads\\or600-priimo_electron-043579706218\\or600-priimo_electron-043579706218\\firstAppElectron\\index.html');
	win2.loadFile('C:\\Users\\or\\Downloads\\or600-priimo_electron-043579706218\\or600-priimo_electron-043579706218\\firstAppElectron\\index.html');

	win.webContents.openDevTools();
	fs.writeFile('C:\\Users\\or\\Downloads\\or600-priimo_electron-043579706218\\or600-priimo_electron-043579706218\\firstAppElectron\\log\\log.txt', data, err => {
		if (err) throw err;
		console.log('The file has been saved!');
	});

	win.on('closed', () => {
		win = null;
		console.log('closed');
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	console.log('window-all-closed');
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (win === null) {
		console.log('activate');
		createWindow();
	}
});
