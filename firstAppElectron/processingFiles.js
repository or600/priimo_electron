const fs = require('fs');
const config = require('config');

const fetch = require("node-fetch");

module.exports=  sendPatientData= async ({patient_id,test_fields,date,test_type})=>{
    try{
        const res = await fetch(`${config.get('PRIIMO_HOST_ADDRESS')}/sendPatientData`,
        {method:'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          token: config.get('PRIIMO_HOST_TOKEN'),
          clinic_id: config.get('CLINIC_ID'),
          date,
          patient_id,
          test_type,
          test_fields
        })
      })
        const data= await res.json()
        
        return data.status
       
    }
    catch(e){
        throw new Error(e)
    }
        
}


const patientList= [
	{ name: 'oren', id: '23434545' },
	{ name: 'dani', id: '12345678' },
	{ name: 'or', id: '78555252' },
	{ name: 'dor', id: '11234345' },
	{ name: 'moshe', id: '13425357' },
	{ name: 'alex', id: '342225654' }
]

const parse_Hl7_To_JSON = (hl7_file)=>{
  const data = {patient_id:"31123", date:'27-08-19', test_fields:{t:3,y:5}}

  sendPatientData(data).then((status)=>{
    console.log(status)

  })



}
const processingFiles =  (patientList)=>{
  try{
     fs.readdir(config.get('SHARED_DIR_LOCATIONS'), async (err, data) => {
      if (err) throw err;
      console.log(data);
     
        for (let f of data){
          fs.readFile(`${config.get('SHARED_DIR_LOCATIONS')}/${f}`,"utf8",(err, content) => {
            if (err) throw err;
            
            for (let patient of patientList){
              if(patient.id === content){
                console.log(content)
                parse_Hl7_To_JSON(`${config.get('SHARED_DIR_LOCATIONS')}/${f}`)
              }

            }
      
        })
       }
      }
    )
  }
  catch(e){
    console.log(e)
  }
 
}
processingFiles(patientList)