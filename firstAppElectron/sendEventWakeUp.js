const config = require('config');
const fetch = require('node-fetch');
const axios = require('axios');

module.exports = sendEventWakeUp = async () => {
	try {
		const res = await fetch(`${config.get('PRIIMO_HOST_ADDRESS')}/sendEvent`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				token: config.get('PRIIMO_HOST_TOKEN'),
				clinic_id: config.get('CLINIC_ID'),
				event_type: 'WAKE_UP',
				event_description: 'send wake up event to the priimo system'
			})
		});
	
		const data = await res.json();

		return data.status;
	} catch (e) {
		throw new Error(e);
	}
};
