const { app, BrowserWindow } = require('electron');
if (process.versions && process.versions.electron) {
	process.env.NODE_CONFIG_DIR = require('electron').app.getAppPath() + '/config';
}
const {fetchData} = require('./apiData')
const config = require('config');
let data = require('./dataConfigurationFile');
let getPatientList = require('./getPatientList');
let sendEventWakeUp = require('./sendEventWakeUp');


const fs = require('fs');
fetchData()


let win;

function createWindow() {
	win = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			nodeIntegration: true
		}
	});

	// getPatientList()
	// 	.then(res => {
	// 		console.log(res, 'hafi');
	// 	})
	// 	.catch(err => {
	// 		app.quit();
	// 	});

	win.loadFile('index.html');

	win.webContents.openDevTools();
	fs.writeFile('log/log.txt', data, err => {
		if (err) throw err;
		console.log('The file has been saved!');
	});

	win.on('closed', () => {
		win = null;
		console.log('closed');
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	console.log('window-all-closed');
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (win === null) {
		console.log('activate');
		createWindow();
	}
});
