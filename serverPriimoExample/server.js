const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;
const authToken = 'ETTDGWErfadf234523542DFDFDS';
const patientList = [
	{ name: 'oren', id: '23434545' },
	{ name: 'dani', id: '12345678' },
	{ name: 'or', id: '78555252' },
	{ name: 'dor', id: '11234345' },
	{ name: 'moshe', id: '13425357' },
	{ name: 'alex', id: '342225654' }
];

app.use(express.json());
//app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.send({ hello: 'hello' });
});

app.post('/sendEvent', (req, res) => {
	console.log(req.body);
	console.log('before if');
	if (
		req.body.token === authToken &&
		req.body.clinic_id !== undefined &&
		req.body.event_type !== undefined &&
		req.body.event_description !== undefined
	) {
		console.log('in if');
		return res.status(200).json({ status: 'OK', payload: {}, error_message: undefined });
	}
	res
		.status(400)
		.send({ status: 'ERROR', payload: undefined, error_message: 'User not authorized' });

});
app.post('/sendPatientData', (req, res) => {
	
	if (
		req.body.token === authToken &&
		req.body.clinic_id !== undefined &&
		req.body.patient_id !== undefined &&
		req.body.test_type !== undefined &&
		req.body.test_fields !== undefined &&
		req.body.date !== undefined
	) {
	   console.log(req.body)
		return res.status(200).json({ status: 'OK', payload: {}, error_message: undefined });
	}
	res
		.status(400)
		.send({ status: 'ERROR', payload: undefined, error_message: 'User not authorized or one of the fields missing' });


});
app.get('/getPatientsList/:auth', (req, res) => {
	if (req.params.auth === authToken) {
		return res.status(200).send({ status: 'OK', payload: patientList, error_message: undefined });
	}
	res
		.status(400)
		.send({ status: 'ERROR', payload: undefined, error_message: 'User not authorized' });
});
app.listen(port, () => {
	console.log('Server is up on port ' + port);
});
