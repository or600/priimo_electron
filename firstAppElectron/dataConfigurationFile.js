const config = require('config');

module.exports = `
CLINIC_ID: ${config.get('CLINIC_ID')}
WAKEUP_INTERVAL: ${config.get('WAKEUP_INTERVAL')}
PRIIMO_HOST_ADDRESS: ${config.get('PRIIMO_HOST_ADDRESS')}
PRIIMO_HOST_TOKEN: ${config.get('PRIIMO_HOST_TOKEN')}
SHARED_DIR_LOCATIONS: ${config.get('SHARED_DIR_LOCATIONS')};
PROVIDER_NAME: ${config.get('PROVIDER_NAME')}
TRASH_DIR: ${config.get('TRASH_DIR')}
RETRY_PERIOD: ${config.get('RETRY_PERIOD')}
CLEAR_SHARED_DIR: ${config.get('CLEAR_SHARED_DIR')}
FILES_MASK: ${config.get('FILES_MASK')}
LOG_FILE_RETENTION_PERIOD: ${config.get('LOG_FILE_RETENTION_PERIOD')}
`;