const config = require('config');
const fetch = require("node-fetch");

module.exports=  getPatientList = async ()=>{
    try{
        const res = await fetch(`${config.get('PRIIMO_HOST_ADDRESS')}/getPatientsList/${config.get('PRIIMO_HOST_TOKEN')}`,
        {method:'GET'})
        const data= await res.json()
        if(data.status === 'OK'){
                return data.payload
        }
       return data.error_message
    }
    catch(e){
        throw new Error(e)
    }
        
}


