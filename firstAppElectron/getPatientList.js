const config = require('config');
const fetch = require("node-fetch");

module.exports=  getPatientList = async ()=>{
    try{
        const res = await fetch(`${config.get('PRIIMO_HOST_ADDRESS')}/getPatientsList/${config.get('PRIIMO_HOST_TOKEN')}`,
        {method:'GET'})
        const data= await res.json()
        
        return data.payload
       
    }
    catch(e){
        throw new Error(e)
    }
        
}


